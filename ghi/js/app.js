function createCard(title, description, pictureUrl, start_date, end_date, location) {
    return `
      <div class="card" style="box-shadow: 10px 10px 5px grey; margin-bottom: 50px" >
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-location" style="color: grey">${location}</h6>
          <p class="card-text">${description}</p>
          <footer class="card-footer">${start_date}-${end_date}</footer>
        </div>
      </div>
    `;
  }




// async is necessary for the await fecth or you will get a syntax error unresoved word
// You can't use await unless the function is an async function
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    // error handling
    try {
        //fetch returns a promise use await to resolve the fetch
        const response = await fetch(url);

        if(!response.ok){
            // Figure out what to do when the response is bad
            window.alert("Response is bad")
        } else{
            //to get the value from the JSON-formatted data that is in the response
            //make sure to type the await keyword because it's a Promise that is being returned
            const data = await response.json();

            // // to get the first conference out of the array
            // const conference = data.conferences[0];
            // //CSS query selector to get a reference to that tag
            // const nameTag = document.querySelector('.card-title');
            // //innerHTML property of the element to set the name of the conference to the content of that element
            // nameTag.innerHTML = conference.name;
            for (let i = 0; i < data.conferences.length; i++){
                //this is a loop for each conference in the  list conferences data
                for (let conference of data.conferences){

                        // this url is for a specific conf to get the detail check insomnia
                        const detailUrl =`http://localhost:8000${conference.href}`;
                        const detailResponse = await fetch(detailUrl);
                        if (detailResponse.ok) {
                            // pulling json data
                            const details = await detailResponse.json();
                            console.log(details);
                            const title = details.conference.name;
                            const description = details.conference.description;
                            const pictureUrl = details.conference.location.picture_url;
                            const start_date = new Date(details.conference.starts).toLocaleDateString();
                            const end_date = new Date(details.conference.ends).toLocaleDateString();
                            const location = details.conference.location.name;
                            const html = createCard(title, description, pictureUrl, start_date, end_date, location);

                            //// create a var to represent accessing that data  spcifically the conference instance
                            // const conferenceDetail = details.conference;
                            // const detailTag = document.querySelector('.card-text');
                            // const imageTag = document.querySelector('.card-img-top')
                            // // using that variable/data for the instance of conference data , we want the description
                            // detailTag.innerHTML = conferenceDetail.description;
                            // imageTag.src = details.conference.location.picture_url;
                            const column = document.querySelectorAll('.col');
                            column[i % 3].innerHTML += html;
                            i++;


                }
                }
            }
        }

    } catch (e){
        console.error(e);
        //Figure out what to do if an error is raised
        window.alert(e)
    }

});
