window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

        const divTag = document.getElementById('loading-conference-spinner');
        divTag.classList.add("d-none");
        selectTag.classList.remove("d-none");


    }
    const formTag = document.getElementById("create-attendee-form");
        formTag.addEventListener('submit', async event =>{
            event.preventDefault();

            //take the form element and create a new FormData object from it
            const formData = new FormData(formTag);
            //turn that FormData object into JSON
            const json =JSON.stringify(Object.fromEntries(formData));

            const attendeeUrl = 'http://localhost:8001/api/attendees/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {'Content-Type': 'application/json',
            },
            };
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok){
            formTag.reset();
            const newLocation = await response.json();


            const successTag = document.getElementById('success-message');
            successTag.classList.remove('d-none');
            formTag.classList.add('d-none')
        }

        });

  });
