import Nav from "./Nav";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import AttendConferenceForm from "./AttendConferenceForm";
import ConferenceForm  from "./ConferenceForm";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";
import {BrowserRouter, Routes, Route, NavLink} from 'react-router-dom';


function App(props) {
  if (props.attendees ===undefined) {
    return null;
  }
  return (

    <BrowserRouter>
    <Nav />
      <div className="container" >
        <Routes>
            <Route index path="/" element={<MainPage />} />
            <Route path="conferences/new" element={<ConferenceForm />} />
            <Route path="attendees/new" element={<AttendConferenceForm />} />
            <Route path="locations/new" element={<LocationForm />} />
            <Route path="presentations/new" element={<PresentationForm />} />
            {/* func.component  (variable passed to attenlist)
            which equals value of something from attendees list */}
            <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
        </Routes>
      </div>
    </BrowserRouter>

  );
}

export default App;
